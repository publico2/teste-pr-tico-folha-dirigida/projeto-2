
import constants from '../config/constants';
import axios from 'axios';

export default async function getCoursesIds(): Promise<number[]> {

    const response = await axios.post<{data: {courses: {id: number}[]}}>(
        constants.coursesApiUri,
        {
            query: `query { courses { id } }`,
        },
        {
            headers: { 'Content-Type': 'application/json' }

        }
    );
    if (response.status !== 200) {
        throw new Error("Internal server error. Try again latter.");
    }

    const data = response.data.data.courses.map(x => x.id);
    return data;
}