import { GraphQLResolveInfo } from "graphql";
import { fieldsList as _fieldsList } from "graphql-fields-list";

export default function fieldsList<T>(info: GraphQLResolveInfo): (keyof T)[] {
    const fields = _fieldsList(info);
    if (!fields.includes('id')) {
        fields.push('id');
    }
    return fields as (keyof T)[];
}