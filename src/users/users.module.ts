import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserCourse } from './entities/user-course.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserCourse])
  ],
  providers: [UsersResolver, UsersService],
  exports: [UsersResolver, UsersService]
})
export class UsersModule {}
