import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import getCoursesIds from '../helpers/getCoursesIds';
import { FindManyOptions, Repository } from 'typeorm';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { UserCourse } from './entities/user-course.entity';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(UserCourse) private userCourseRepository: Repository<UserCourse>
  ) { }

  async create(createUserInput: CreateUserInput): Promise<User> {
    const newUser = this.userRepository.create(createUserInput);
    return await this.userRepository.save(newUser);
  }

  async findAll(options?: FindManyOptions<User>): Promise<User[]> {
    return await this.userRepository.find(options);
  }

  async findOne(id: number, options?: FindManyOptions<User>): Promise<User> {
    return await this.userRepository.findOne(id, options);
  }

  async update(id: number, updateUserInput: UpdateUserInput): Promise<User> {
    return await this.userRepository.manager.transaction(async transaction => {
      const user = await transaction.findOne(User, id);
      if (!user) throw new Error(`User not found`);
      Object.assign(user, updateUserInput);
      const updatedUser = await transaction.save(user);
      return updatedUser;
    });
  }

  async remove(id: number): Promise<User> {
    return await this.userRepository.manager.transaction(async transaction => {
      const user = await transaction.findOne(User, id);
      if (!user) throw new Error(`User not found`);
      const removed = await transaction.remove(user);
      removed.id = id;
      return removed;
    });
  }

  async getUserCourses(user_id: number): Promise<UserCourse[]> {
    const user = await this.userRepository.findOne(user_id, {
      relations: ['user_courses'],
      loadRelationIds: false
    });
    if (!user) throw new Error(`User not found`);
    console.log(user.user_courses);
    return user.user_courses;
  }

  async getUserWithUserCourses(user_id: number): Promise<User> {
    const user = await this.userRepository.findOne(user_id, {
      relations: ['user_courses'],
      loadRelationIds: false
    });
    if (!user) throw new Error(`User not found`);
    return user;
  }

  async addUserCourse(user_id: number, course_id: number): Promise<void> {
    const user = await this.getUserWithUserCourses(user_id);
    if (user.user_courses.map(x => x.id_course).includes(course_id)) {
      throw new Error('This course is already binded to this user');
    }
    const allowedCoursesIds = await getCoursesIds();
    if (!allowedCoursesIds.includes(course_id)) {
      throw new Error('This course is not valid');
    }
    const newUserCourse = this.userCourseRepository.create({
      id_course: course_id,
      id_user: user_id
    });
    await this.userCourseRepository.save(newUserCourse);
  }

  async removeUserCourse(user_id: number, course_id: number): Promise<void> {
    const user = await this.getUserWithUserCourses(user_id);
    console.log(user.user_courses);
    const userCourse = user.user_courses.find(x => x.id_course === course_id);
    if (!userCourse) {
      throw new Error('This course is not binded to this user');
    }
    await this.userCourseRepository.remove(userCourse);
  }
}
