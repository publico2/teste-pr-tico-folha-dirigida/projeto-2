import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateUserInput {
  @Field(() => String, { nullable: false })
  first_name: string;

  @Field(() => String, { nullable: false })
  last_name: string;

  @Field(() => String, { nullable: false })
  email: string;
}
