import { CreateUserInput } from './create-user.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @Field(() => String, { nullable: false })
  first_name: string;

  @Field(() => String, { nullable: false })
  last_name: string;

  @Field(() => String, { nullable: false })
  email: string;
}
