import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@ObjectType()
@Entity()
export class UserCourse {
  @Field(() => Int, { description: 'Chave primária', nullable: false })
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => Int, { nullable: false })
  @Column("int")
  id_course: number;

  @Column("int", { nullable: false })
  id_user: number;

  @ManyToOne(() => User, user => user.user_courses, {onDelete: 'CASCADE'})
  @JoinColumn({name: 'id_user'})
  user: User;
}
