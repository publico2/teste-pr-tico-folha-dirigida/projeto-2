import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserCourse } from './user-course.entity';

@ObjectType()
@Entity()
export class User {
  @Field(() => Int, { description: 'Chave primária', nullable: false })
  @PrimaryGeneratedColumn()
  id: number;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 100})
  first_name: string;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 100})
  last_name: string;

  @Field(() => String, { nullable: false })
  @Column("varchar", {length: 255})
  email: string;

  @OneToMany(() => UserCourse, userCourse => userCourse.user, {onDelete: 'CASCADE'}) 
  user_courses: UserCourse[];
}
