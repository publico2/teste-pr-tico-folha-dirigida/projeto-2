import { Resolver, Query, Mutation, Args, Int, Info, ResolveField, Parent } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { GraphQLResolveInfo } from 'graphql';
import fieldsList from '../helpers/fieldsList';
import { UserCourse } from './entities/user-course.entity';

@Resolver(() => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Mutation(() => User)
  createUser(@Args('createUserInput') createUserInput: CreateUserInput) {
    return this.usersService.create(createUserInput);
  }

  @Query(() => [User], { name: 'users' })
  findAll(
    @Info() info: GraphQLResolveInfo
  ) {
    const attributes = fieldsList(info);
    return this.usersService.findAll({select: attributes});
  }

  @Query(() => User, { name: 'user' })
  findOne(
    @Info() info: GraphQLResolveInfo,
    @Args('id', { type: () => Int }) id: number
  ) {
    const attributes = fieldsList(info);
    return this.usersService.findOne(id, {select: attributes});
  }

  @Mutation(() => User)
  updateUser(
    @Args('id', {type: () => Int}) id: number, 
    @Args('updateUserInput') updateUserInput: UpdateUserInput
  ) {
    return this.usersService.update(id, updateUserInput);
  }

  @Mutation(() => User)
  removeUser(@Args('id', { type: () => Int }) id: number) {
    return this.usersService.remove(id);
  }

  @ResolveField(() => [UserCourse])
  async user_courses(@Parent() user: User) {
    return this.usersService.getUserCourses(user.id);
  }

  @Mutation(() => Boolean)
  async addUserCourseFromUser(
    @Args('user_id', { type: () => Int }) user_id: number,
    @Args('course_id', { type: () => Int }) course_id: number
  ): Promise<boolean> {
    await this.usersService.addUserCourse(user_id, course_id);
    return true;
  }

  @Mutation(() => Boolean)
  async removeUserCourseFromUser(
    @Args('user_id', { type: () => Int }) user_id: number,
    @Args('course_id', { type: () => Int }) course_id: number
  ): Promise<boolean> {
    await this.usersService.removeUserCourse(user_id, course_id);
    return true;
  }
}
