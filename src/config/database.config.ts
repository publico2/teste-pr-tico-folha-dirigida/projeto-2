import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { UserCourse } from "src/users/entities/user-course.entity";
import { User } from "src/users/entities/user.entity";

const databaseConfig: TypeOrmModuleOptions = {
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "postgres",
    password: "admin",
    database: "folha-dirigida-projeto2",
    synchronize: true,
    entities: [User, UserCourse],
    logging: true
};

export default databaseConfig;