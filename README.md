

## Descrição

Este projeto foi desenvolvido como parte do processo seletivo para a vaga de desenvolvedor Node.JS para a Folha Dirigida.

O teste prático é composto de dois projetos:
- Projeto 1: API para controle de CURSOS (este projeto).
- Projeto 2: API para controle de ALUNOS e seu relacionamento com os CURSOS do Projeto 1 (este projeto).

## Tecnologias

Este projeto utiliza o banco de dados PostgreSQL para a persistência e o framework
[Nest](https://github.com/nestjs/nest) para a contrução da API em ambiente Node.JS utilizando a linguagem Typescript.

## Instalação

Antes de instalar o sistema verifique se as seguintes dependências estão instaladas e configuradas:
- Node.JS > 12.0
- NPM > 6.0
- PostgresSQL (Opcional na máquina ou pode ser remoto)

Após baixar, basta navegar até a pasta do projeto e executar o comando:
```bash
$ npm install
```

Para configurar o banco de dados, modifique o arquivo src/config/database.config.ts

## Executando o sistema

```bash
# modo de desenvolvimento
$ npm run start

# modo de desenvolvimento com auto refresh
$ npm run start:dev

# modo de produção
$ npm run start:prod
```

## Testes

OBS: Os testes ainda não estão funcionais. Favor ignorar essa seção.

```bash
# teste de unidade
$ npm run test

# teste e2e
$ npm run test:e2e
```

## Autor
Desenvolvido por: Rodrigo Menezes (rodrigo.fsamenezes@gmail.com)
